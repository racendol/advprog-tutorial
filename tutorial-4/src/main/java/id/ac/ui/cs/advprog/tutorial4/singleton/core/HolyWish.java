package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private String wish;

    //Menggunakan Eager Approach.
    //Eager Approach membuat instance dari HolyWish ketika class dibuat.
    //Menggunakan Eager Approach karena ketika di test menggunakan Lazy Approach
    //bisa menghasilkan error ketika test dijalankan, sehingga hasil test menjadi inconsistent.
    //Test gagal dengan Lazy Approach karena Lazy Approach tidak thread-safe.
    private static final HolyWish instance = new HolyWish();

    // TODO complete me with any Singleton approach
    private HolyWish(){
        wish = "nothing";
    }

    public static HolyWish getInstance(){
        return instance;
    }


    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
