package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new LordranAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals(majesticKnight.getName(), "Majestic Knight from Lordran");
        assertEquals(metalClusterKnight.getName(), "Metal Cluster Knight from Lordran");
        assertEquals(syntheticKnight.getName(), "Synthetic Knight from Lordran");

        assertEquals(majesticKnight.getArmor().getName(), "Shining Armor");
        assertEquals(majesticKnight.getWeapon().getName(), "Shining Buster");

        assertEquals(metalClusterKnight.getArmor().getName(), "Shining Armor");
        assertEquals(metalClusterKnight.getSkill().getName(), "Shining Force");

        assertEquals(syntheticKnight.getWeapon().getName(), "Shining Buster");
        assertEquals(syntheticKnight.getSkill().getName(), "Shining Force");
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals(majesticKnight.getArmor().getDescription(), "Armor yang bersinar terang!");
        assertEquals(majesticKnight.getWeapon().getDescription(), "<< SHAININGGU BASTAA >>");

        assertEquals(metalClusterKnight.getArmor().getDescription(), "Armor yang bersinar terang!");
        assertEquals(metalClusterKnight.getSkill().getDescription(), "Rasakan kekuatan sinar bintang!");

        assertEquals(syntheticKnight.getWeapon().getDescription(), "<< SHAININGGU BASTAA >>");
        assertEquals(syntheticKnight.getSkill().getDescription(), "Rasakan kekuatan sinar bintang!");
    }
}
