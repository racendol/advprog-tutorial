package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Test;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;

import static org.mockito.Mockito.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.junit.jupiter.api.BeforeEach;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests
    @Test
    public void whenGetHolyWishIsCalledItShouldReturnHolyWish(){
        HolyGrail holyGrail = new HolyGrail();
        HolyWish calledHolyWish = holyGrail.getHolyWish();
        HolyWish otherHolyWish = HolyWish.getInstance();

        assertThat(calledHolyWish).isEqualTo(otherHolyWish);
    }

    @Test
    public void whenMakeAWishIsCalledItShouldChangeWish(){
        HolyGrail holyGrail = new HolyGrail();
        assertThat(holyGrail.getHolyWish().getWish()).isEqualTo("nothing");
        holyGrail.makeAWish("test");
        assertThat(holyGrail.getHolyWish().getWish()).isEqualTo("test");
    }
}
