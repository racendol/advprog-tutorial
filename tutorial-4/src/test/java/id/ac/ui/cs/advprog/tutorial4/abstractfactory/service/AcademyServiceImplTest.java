package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    // TODO create tests

    @Test
    public void whenProduceKnightIsCalledItShouldHaveAKnightAndCalledGetKnightAcademyByName(){
        academyService = new AcademyServiceImpl(new AcademyRepository());
        assertThat(academyService.getKnightAcademies().size()).isNotEqualTo(0);

        academyService.produceKnight("Lordran", "majestic");

        assertThat(academyService.getKnight()).isNotEqualTo(null);
    }

    @Test
    public void whenGetKnightIsCalledItShouldReturnAKnight(){
        academyService = new AcademyServiceImpl(new AcademyRepository());
        academyService.produceKnight("Lordran", "majestic");
        assertTrue(academyService.getKnight() instanceof Knight);
    }

    @Test
    public void whenGetKnightAcademiesIsCalledItShouldCallGetKnightAcademies(){
        academyService.getKnightAcademies();
        verify(academyRepository, times(1)).getKnightAcademies();
    }
}
