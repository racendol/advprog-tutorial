package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
        //TODO: Complete me
        private String name;
        private String role;

        public OrdinaryMember(String name, String role){
                this.name = name;
                this.role = role;
        }

        @Override
        public void addChildMember(Member member) {
                //do nothing karena ordinary member gak bisa add child
        }

        public void removeChildMember(Member member){
                //do nothing karena ordinary member gak ada child
        }
        
        public List<Member> getChildMembers(){
                return new ArrayList<Member>();
        }

        public String getName(){
                return name;
        }

        public String getRole(){
                return role;
        }
}
