package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member megumin = new OrdinaryMember("Megumin", "EXPLOSION");
        member.addChildMember(megumin);
        assertEquals(megumin, member.getChildMembers().get(0));
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member azuma = new OrdinaryMember("Azuma", "NEET");
        member.addChildMember(azuma);
        member.removeChildMember(azuma);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member megumin = new OrdinaryMember("Megumin", "EXPLOSION");
        Member azuma = new OrdinaryMember("Azuma", "NEET");
        Member darkness = new OrdinaryMember("Darkness", "Tank");
        
        member.addChildMember(megumin);
        member.addChildMember(azuma);
        member.addChildMember(darkness);

        assertEquals(3, member.getChildMembers().size());

        Member yunyun = new OrdinaryMember("Yunyun", "Cute");
        member.addChildMember(yunyun);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member kirito = new PremiumMember("Kirito", "Master");

        Member asuna = new OrdinaryMember("Asuna", "Waifu");
        Member suguha = new OrdinaryMember("Suguha", "Imouto");
        Member sinon = new OrdinaryMember("Sinon", "Sniper");
        Member alice = new OrdinaryMember("Alice", "Waifu");

        kirito.addChildMember(asuna);
        kirito.addChildMember(suguha);
        kirito.addChildMember(sinon);
        kirito.addChildMember(alice);

        assertEquals(4, kirito.getChildMembers().size());

    }
}
