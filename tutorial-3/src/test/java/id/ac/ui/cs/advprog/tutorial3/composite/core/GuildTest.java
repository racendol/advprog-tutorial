package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        Member peco = new PremiumMember("Peco", "Member");

        guild.addMember(guildMaster, peco);
        assertEquals(peco, guild.getMemberList().get(1));
        assertEquals(peco, guildMaster.getChildMembers().get(0));
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member karyl = new OrdinaryMember("Karyl", "Traitor");

        guild.addMember(guildMaster, karyl);
        
        guild.removeMember(guildMaster, karyl);
        assertEquals(1, guild.getMemberList().size());
        assertEquals(0, guildMaster.getChildMembers().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member dookers = new PremiumMember("Dook", "Idol");

        guild.addMember(guildMaster, dookers);
        assertEquals(dookers, guild.getMember("Dook", "Idol"));
    }
}
