// TODO: Import service bean
package id.ac.ui.cs.advprog.tutorial5.service;

import org.springframework.stereotype.Service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SoulServiceImpl implements SoulService {
    // TODO: implementasi semua method di SoulService.java. Coba lihat dokumentasi JpaRepository untuk mengimplementasikan Service
    private final SoulRepository soulRepository;

    public SoulServiceImpl(SoulRepository soulRepository){
        this.soulRepository = soulRepository;
    }

    @Override
    public List<Soul> findAll(){
        return soulRepository.findAll();
    }

    public Optional<Soul> findSoul(Long id){
        return soulRepository.findById(id);
    }

    public void erase(Long id){
        soulRepository.deleteById(id);
    }

    public Soul rewrite(Soul soul){
        return soulRepository.save(soul);
    }

    public Soul register(Soul soul){
        return soulRepository.save(soul);
    }
}
