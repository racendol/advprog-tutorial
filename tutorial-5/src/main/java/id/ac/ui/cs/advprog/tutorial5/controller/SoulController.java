// TODO: Import apapun yang anda perlukan agar controller ini berjalan
package id.ac.ui.cs.advprog.tutorial5.controller;

import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import id.ac.ui.cs.advprog.tutorial5.core.Soul;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;
import java.util.Optional;



@RestController
@RequestMapping(path = "/soul")
public class SoulController {

	@Autowired
    private final SoulService soulService;
    
    public SoulController(SoulService soulService){
        this.soulService = soulService;
    }

    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        // TODO: Use service to complete me.
        return new ResponseEntity<List<Soul>>(soulService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Soul soul) {
        // TODO: Use service to complete me.
        Optional<Soul> originalSoul = soulService.findSoul(soul.getId());
        if (originalSoul.isPresent()) {
            return new ResponseEntity<>("FAILED: Soul with ID: "+soul.getId()+ " is already in the database.", HttpStatus.BAD_REQUEST);
        } else {
            soulService.register(soul);
            return new ResponseEntity<>("SUCCESS: Soul with ID: "+soul.getId()+ " is succesfully created.", HttpStatus.OK);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id) {
        // TODO: Use service to complete me.
        Optional<Soul> soul = soulService.findSoul(id);

        if (soul.isPresent()) {
            return new ResponseEntity<Soul>(soul.get(), HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        
    }
	
	@PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        // TODO: Use service to complete me.
        Optional<Soul> optOriginalSoul = soulService.findSoul(id);
        if (optOriginalSoul.isPresent()) {
            Soul originalSoul = optOriginalSoul.get();
            originalSoul.setAge(soul.getAge());
            originalSoul.setGender(soul.getGender());
            originalSoul.setName(soul.getName());
            originalSoul.setOccupation(soul.getOccupation());
            return new ResponseEntity<Soul>(soulService.rewrite(originalSoul), HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        // TODO: Use service to complete me.
        Optional<Soul> originalSoul = soulService.findSoul(id);
        if (originalSoul.isPresent()) {
            soulService.erase(id);
            return new ResponseEntity<>("SUCCESS: Soul with ID: "+id+ " is succesfully deleted.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("FAILED: Soul with ID: "+id+ " is not in the database.", HttpStatus.BAD_REQUEST);
        }
    }
}
