package id.ac.ui.cs.advprog.tutorial5.controller;

import org.junit.jupiter.api.Test;
import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;


@WebMvcTest(controllers = SoulController.class)
public class SoulControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SoulServiceImpl soulService;

    @Test
    public void testFindAll() throws Exception {
        mockMvc.perform(get("/soul"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andDo(new ResultHandler(){
            
                @Override
                public void handle(MvcResult result) throws Exception {
                    //Empty database
                    assertEquals("[]", result.getResponse().getContentAsString());
                }
            });
    }

    @Test
    public void testCreate() throws Exception {
        Soul soul = new Soul(1, "test", 100, "tesgen", "tesocc");
        String jsonSoul = new ObjectMapper().writeValueAsString(soul);

        mockMvc.perform(post("/soul").content(jsonSoul).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andDo(new ResultHandler(){
            
                @Override
                public void handle(MvcResult result) throws Exception {
                    assertEquals("SUCCESS: Soul with ID: 1 is succesfully created.", result.getResponse().getContentAsString());
                }
            });

    }

    @Test
    public void testFindById() throws Exception{
        mockMvc.perform(get("/soul/2"))
            .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdate() throws Exception {
        Soul wrongSoul = new Soul(2, "test", 100, "tesgen", "tesocc");
        String jsonWrongSoul = new ObjectMapper().writeValueAsString(wrongSoul);
        mockMvc.perform(put("/soul/1").content(jsonWrongSoul).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
    }

    @Test
    public void testDelete() throws Exception {
        Soul soul = new Soul(1, "test", 100, "tesgen", "tesocc");

        mockMvc.perform(delete("/soul/1"))
            .andExpect(status().isBadRequest())
            .andDo(new ResultHandler(){
            
                @Override
                public void handle(MvcResult result) throws Exception {
                    assertEquals("FAILED: Soul with ID: 1 is not in the database.", result.getResponse().getContentAsString());
                }
            });
    }
}