package id.ac.ui.cs.advprog.tutorial5.core;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoulTest {

    Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new Soul(1, "testname", 99, "testgender", "testoccupation");
    }

    @Test
    public void testGetterMethod(){
       assertEquals(soul.getId(), 1);
       assertEquals(soul.getName(), "testname");
       assertEquals(soul.getAge(), 99);
       assertEquals(soul.getGender(), "testgender");
       assertEquals(soul.getOccupation(), "testoccupation");
    }

    @Test
    public void testSetterMethod(){
        soul.setName("sachiko");
        soul.setAge(1010);
        soul.setGender("female");
        soul.setOccupation("idol");

        assertEquals(soul.getId(), 1);
        assertEquals(soul.getName(), "sachiko");
        assertEquals(soul.getAge(), 1010);
        assertEquals(soul.getGender(), "female");
        assertEquals(soul.getOccupation(), "idol");
    }
}